# fullstack-frontend

# RUNNING

1) Download the back-end part here https://bitbucket.org/katevorona/fullstack-backend

2) Run back-end part (Readme.md is provided there)

3) Install all dependencies `npm install`

4) Run this app with command `npm run start`

