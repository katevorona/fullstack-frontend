import {
  LOAD_TRANSACTIONS,
  LOAD_TRANSACTIONS_FAIL,
  LOAD_TRANSACTIONS_SUCCESS,
  CREATE_TRANSACTION_FAIL,
  CREATE_TRANSACTION_SUCCESS
} from '../constants/actions'

const initialState = {
    list: [],
    errorMessage: null
};

export default function transactions(state = initialState, action) {
    switch (action.type) {
        case LOAD_TRANSACTIONS:
        case LOAD_TRANSACTIONS_FAIL: {
          return {
                ...state,
                list: [],
                errorMessage: null
            };
        }
        case LOAD_TRANSACTIONS_SUCCESS: {
            return {
                ...state,
                list: action.data,
                errorMessage: null
            };
        }
        case CREATE_TRANSACTION_SUCCESS: {
            const newList = [...state.list];
            newList.unshift(action.payload);

            return {
                ...state,
                errorMessage: null,
                list: newList
            }
        }
        case CREATE_TRANSACTION_FAIL: {
            return {
                ...state,
                errorMessage: `Transaction cannot be created: ${action.message}`
            }
        }

        default: {
            return state
        }
    }
}
