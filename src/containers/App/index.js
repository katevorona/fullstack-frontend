import React from 'react'
import './styles.css'

const App = (params) => (
  <div>
    {params.children}
  </div>
);

export default App
