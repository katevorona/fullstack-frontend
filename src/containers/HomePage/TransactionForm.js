import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {
    Input,
    Select,
    DateTime
} from '../../components/FormElements';
import { TRANSACTION_TYPES_ENUM, TRANSACTION_FIELDS_ENUM } from './../../constants'
import './styles.css';


const DEFAULT_TRANSACTION = {
    id: '',
    type: null,
    amount: '',
    effectiveDate: ''
};

const TransactionForm = ({handleSubmit, errorMessage, transactionsCount}) => {
        const [transaction, setTransaction] = useState(DEFAULT_TRANSACTION);

        const handleChange = (field, value) => {
            setTransaction({
                ...transaction,
                [field]: value
            });
        };

        //handle form successfully submitted
        useEffect(() => setTransaction(DEFAULT_TRANSACTION), [transactionsCount]);

        return (
            <div className='transaction-form'>
                <h4>Add transaction</h4>

                {!!errorMessage &&
                <div className="transaction-form-error">{errorMessage}</div>
                }

                <form className='add-transaction-form'>
                    <div className="transaction-form-field">
                        <label>Id</label>
                        <Input
                            type="text"
                            name={TRANSACTION_FIELDS_ENUM.id}
                            value={transaction[TRANSACTION_FIELDS_ENUM.id]}
                            onChange={(value) => handleChange(TRANSACTION_FIELDS_ENUM.id, value)}
                        />
                    </div>
                    <div className="transaction-form-field">
                        <label>Type</label>
                        <Select
                            name={TRANSACTION_FIELDS_ENUM.type}
                            value={transaction[TRANSACTION_FIELDS_ENUM.type]}
                            values={TRANSACTION_TYPES_ENUM}
                            onChange={(value) => handleChange(TRANSACTION_FIELDS_ENUM.type, value)}
                        />
                    </div>
                    <div className="transaction-form-field">
                        <label>Amount</label>
                        <Input
                            type="number"
                            name={TRANSACTION_FIELDS_ENUM.amount}
                            value={transaction[TRANSACTION_FIELDS_ENUM.amount]}
                            onChange={(value) => handleChange(TRANSACTION_FIELDS_ENUM.amount, Number(value))}
                        />
                    </div>
                    <div className="transaction-form-field">
                        <label>Effective Date</label>
                        <DateTime
                            name={TRANSACTION_FIELDS_ENUM.effectiveDate}
                            value={transaction[TRANSACTION_FIELDS_ENUM.effectiveDate]}
                            onChange={(value) => handleChange(TRANSACTION_FIELDS_ENUM.effectiveDate, value)}
                        />
                    </div>

                    <button
                        type="button"
                        className='transaction-form-button'
                        onClick={() => handleSubmit(transaction)}
                    >
                        Create Transaction
                    </button>
                </form>
            </div>
        );
    }
;

TransactionForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    errorMessage: PropTypes.string
};

export default TransactionForm;
