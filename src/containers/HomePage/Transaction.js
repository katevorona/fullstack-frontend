import React, {useState, useMemo} from 'react';
import moment from 'moment';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const CREDIT_TYPE = 'credit';
const DEBIT_TYPE = 'debit';

const Transaction = ({
       id,
       amount,
       type,
       effectiveDate
   }) => {
    const [isOpen, setOpen] = useState(false);

    return useMemo(() => (
        <div
            key={id}
            className={classnames('transaction-item', {
                'credit-transaction': type === CREDIT_TYPE,
                'debit-transaction': type === DEBIT_TYPE,
                'active': isOpen
            })}
            onClick={() => {
                setOpen(!isOpen)
            }}
        >
            <div className='transaction-item__transaction-title'>
            <span>
              <span className="arrow"/>
                {id}
            </span>
                <span>{type}</span>
            </div>
            {isOpen &&
            <div className='transaction-item__transaction-fullinfo'>
                <div><span> ID: </span> {id} </div>
                <div><span> Amount: </span> {amount} </div>
                <div><span> Type: </span> {type} </div>
                <div><span> Effective Date: </span> {moment(effectiveDate).format('Do MMM YYYY')} </div>
            </div>
            }
        </div>
    ), [
        id,
        amount,
        type,
        effectiveDate,
        isOpen
    ]);
};

Transaction.propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    effectiveDate: PropTypes.string.isRequired
};

export default Transaction;
