import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getTransactions, createTransaction} from '../../actions/transactions';
import Transaction from './Transaction';
import TransactionForm from './TransactionForm';
import './styles.css';

const Home = () => {
    const dispatch = useDispatch();
    const transactions = useSelector(state => state.transactions.list);
    const errorMessage = useSelector(state => state.transactions.errorMessage);

    useEffect(() => {
        dispatch(getTransactions());
    }, []);

    const handleCreateTransaction = (transaction) => {
        dispatch(createTransaction(transaction));
    };

    return (
      <div className='transaction-page'>
          <h1> User transactions</h1>
          <div className='transaction-page-container'>
              <div className='transaction-add-form'>
                    <TransactionForm
                        transactionsCount={transactions.length}
                        errorMessage={errorMessage}
                        handleSubmit={handleCreateTransaction} />
                </div>
                <div className='transaction-list'>
                    {transactions.map((transaction) =>
                        <Transaction key={transaction.id} {...transaction} />
                    )}
                </div>
            </div>
        </div>
    );
};

export default Home;
