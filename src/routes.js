import React from 'react'
import { Route } from 'react-router'
import App from './containers/App'
import HomePage from './containers/HomePage'

const routes = (
  <Route component={App}>
    <Route path="/" component={HomePage} />
  </Route>
)

export default routes
