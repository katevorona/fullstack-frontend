export const API_URL = 'http://localhost:3003';

export const TRANSACTION_TYPES_ENUM = [
  {label: 'Credit', value: 'credit'},
  {label: 'Debit', value: 'debit'},
];

export const TRANSACTION_FIELDS_ENUM = {
  id: 'id',
  type: 'type',
  amount: 'amount',
  effectiveDate: 'effectiveDate'
};
