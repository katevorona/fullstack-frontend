import { API_URL } from '../constants'
import {
  LOAD_TRANSACTIONS,
  LOAD_TRANSACTIONS_FAIL,
  LOAD_TRANSACTIONS_SUCCESS,
  CREATE_TRANSACTION_FAIL,
  CREATE_TRANSACTION_SUCCESS
} from '../constants/actions'
import axios from 'axios';

export function getTransactions() {
  return (dispatch) => {
    dispatch({ type: LOAD_TRANSACTIONS });

    axios.get(`${API_URL}/transactions`)
      .then((response) => (
        dispatch({
          type: LOAD_TRANSACTIONS_SUCCESS,
          data: response.data
        })
      ))
      .catch((err) => (
        dispatch({
          type: LOAD_TRANSACTIONS_FAIL
        })
      ))
  }
}

export function createTransaction(transaction) {
  return (dispatch) => {
    axios.post(`${API_URL}/transactions`, transaction)
      .then((response) => {
        dispatch({
          type: CREATE_TRANSACTION_SUCCESS,
          payload: transaction
        })
      })
      .catch(function (err) {
        const { data: {error: {message} = {}} = {} } = err.response;
        return dispatch({
          type: CREATE_TRANSACTION_FAIL,
          message: message
        })
      })
  }
}
