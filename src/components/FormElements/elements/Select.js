import React from 'react';
import ReactSelect from 'react-select'

export default function Select({ name, onChange, values }) {
    return (
      <div className='transaction-form-elem'>
        <ReactSelect
          name={name}
          options={values}
          onChange={(e) => {onChange(e.value)}}
        />
      </div>
    );
}
