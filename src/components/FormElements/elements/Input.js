import React from 'react';

export default function Input({name, onChange, value, placeholder, type}) {
  return (
    <div className='transaction-form-elem'>
      <input
        name={name}
        type={type}
        onChange={(e) => onChange(e.target.value)}
        value={value}
        placeholder={placeholder}
      />
    </div>
  );
}
