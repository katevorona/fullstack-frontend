import React from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default function DateTime({ name, onChange, value}) {

  const handleChange = e => {
    const newDate = moment(e).format('YYYY-MM-DD');
    return onChange(newDate);
  };

  return (
    <div className='transaction-form-elem'>
        <DatePicker
          name={name}
          onChange={e => handleChange(e)}
          selected={value ? moment(value).toDate() : null}
          placeholderText="Please select a date"
          dateFormat="do MMM yyyy"
          autoComplete={'off'}
        />
    </div>
  );
}
